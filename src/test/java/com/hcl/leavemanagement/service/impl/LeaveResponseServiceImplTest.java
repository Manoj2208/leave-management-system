package com.hcl.leavemanagement.service.impl;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.hcl.leavemanagement.repository.EmployeeRepository;
import com.hcl.leavemanagement.repository.LeaveRequestRepository;
import com.hcl.leavemanagement.repository.LeaveResponseRepository;
import com.hcl.leavemanagement.repository.LeaveTypeRepository;
import com.hcl.leavemanagement.repository.LeavesRepository;

@ExtendWith(SpringExtension.class)
class LeaveResponseServiceImplTest {
	@Mock
	EmployeeRepository employeeRepository;
	@Mock
	LeaveResponseRepository leaveResponseRepository;
	@Mock
	LeaveRequestRepository leaveRequestRepository;
	@Mock
	LeavesRepository leavesRepository;
	@Mock
	LeaveTypeRepository leaveTypeRepository;
	@InjectMocks
	LeaveResponseServiceImpl responseServiceImpl;
	
	
	
	
}
