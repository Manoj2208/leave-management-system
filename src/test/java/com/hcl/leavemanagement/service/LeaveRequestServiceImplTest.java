package com.hcl.leavemanagement.service;

import java.util.List;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.hcl.leavemanagement.dto.LeaveRequestDto;
import com.hcl.leavemanagement.dto.LeaveTypeDto;
import com.hcl.leavemanagement.entity.Type;
import com.hcl.leavemanagement.repository.EmployeeRepository;
import com.hcl.leavemanagement.repository.LeaveRequestRepository;
import com.hcl.leavemanagement.repository.LeavesRepository;
import com.hcl.leavemanagement.service.impl.LeaveRequestServiceImpl;

@ExtendWith(SpringExtension.class)
class LeaveRequestServiceImplTest {
	@Mock
	private EmployeeRepository employeeRepository;
	@Mock
	private LeavesRepository leavesRepository;
	@Mock
	private LeaveRequestRepository leaveRequestRepository;

	@InjectMocks
	private LeaveRequestServiceImpl leaveRequestServiceImpl;

	void testApplyLeave() {
		LeaveTypeDto leaveTypeDto = LeaveTypeDto.builder().fromDate("2023-01-02").toDate("2023-01-05")
				.leaveType(Type.AnnualLeave).reason("Medical").build();
		List<LeaveTypeDto> leaveTypeDtos = List.of(leaveTypeDto);
		LeaveRequestDto leaveRequestDto = LeaveRequestDto.builder().sapId(123l).leaveRequests(leaveTypeDtos).build();
		
	}
}
