package com.hcl.leavemanagement.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.hcl.leavemanagement.dto.ApiResponse;
import com.hcl.leavemanagement.dto.LeaveRequestDto;
import com.hcl.leavemanagement.dto.LeaveTypeDto;
import com.hcl.leavemanagement.entity.Type;
import com.hcl.leavemanagement.service.LeaveRequestService;

@ExtendWith(SpringExtension.class)
class LeaveRequestControllerTest {
	@Mock
	private LeaveRequestService leaveRequestService;
	@InjectMocks
	private LeaveRequestController leaveRequestController;

	@Test
	void testApplyLeave() {
		LeaveTypeDto leaveTypeDto = LeaveTypeDto.builder().fromDate("2023-01-02").toDate("2023-01-05")
				.leaveType(Type.AnnualLeave).reason("Medical").build();
		List<LeaveTypeDto> leaveTypeDtos = List.of(leaveTypeDto);
		ApiResponse apiResponse=ApiResponse.builder().message("Success").httpStatus(201l).build();
		LeaveRequestDto leaveRequestDto = LeaveRequestDto.builder().sapId(123l).leaveRequests(leaveTypeDtos).build();
		Mockito.when(leaveRequestService.applyLeave(leaveRequestDto))
				.thenReturn(apiResponse);
		ResponseEntity<ApiResponse> responseEntity=leaveRequestController.applyLeave(leaveRequestDto);
		assertNotNull(responseEntity);
		assertEquals(apiResponse.message(), responseEntity.getBody().message());
	}
}
