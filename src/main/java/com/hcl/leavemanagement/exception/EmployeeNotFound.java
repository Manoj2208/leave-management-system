package com.hcl.leavemanagement.exception;

public class EmployeeNotFound extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public EmployeeNotFound(String message) {
		super(message, GlobalErrorCode.ERROR_RESOURCE_NOT_FOUND);
	}

	public EmployeeNotFound() {
		super("Resource Not Found", GlobalErrorCode.ERROR_RESOURCE_NOT_FOUND);
	}

}
