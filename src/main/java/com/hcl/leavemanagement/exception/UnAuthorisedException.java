package com.hcl.leavemanagement.exception;

public class UnAuthorisedException extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UnAuthorisedException(String message) {
		super(message, GlobalErrorCode.ERROR_UNAUTHOURIZED_EMPLOYEE);
	}

	public UnAuthorisedException() {
		super("Access Denied", GlobalErrorCode.ERROR_UNAUTHOURIZED_EMPLOYEE);
	}

}
