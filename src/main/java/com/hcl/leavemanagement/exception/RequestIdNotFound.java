package com.hcl.leavemanagement.exception;

public class RequestIdNotFound extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public RequestIdNotFound(String message) {
		super(message, GlobalErrorCode.ERROR_RESOURCE_NOT_FOUND);
	}

	public RequestIdNotFound() {
		super("Resource Not Found", GlobalErrorCode.ERROR_RESOURCE_NOT_FOUND);
	}

}
