package com.hcl.leavemanagement.exception;

public class LeaveRequestConflictException extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public LeaveRequestConflictException() {
		super("Customer already Registered", GlobalErrorCode.ERROR_RESOURCE_CONFLICT_EXISTS);
	}

	public LeaveRequestConflictException(String message) {
		super(message, GlobalErrorCode.ERROR_RESOURCE_CONFLICT_EXISTS);
	}

}
