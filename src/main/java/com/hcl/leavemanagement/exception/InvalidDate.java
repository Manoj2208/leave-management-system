package com.hcl.leavemanagement.exception;

public class InvalidDate extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidDate(String message) {
		super(message, GlobalErrorCode.ERROR_BAD_REQUEST);
	}

	public InvalidDate() {
		super("Unauthorized Customer", GlobalErrorCode.ERROR_BAD_REQUEST);
	}
}
