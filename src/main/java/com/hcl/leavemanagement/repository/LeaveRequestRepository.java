package com.hcl.leavemanagement.repository;

import java.time.LocalDate;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.leavemanagement.entity.Employee;
import com.hcl.leavemanagement.entity.LeaveRequest;

public interface LeaveRequestRepository extends JpaRepository<LeaveRequest, Long> {
	Optional<LeaveRequest> findByFromDateOrToDateAndEmployee(LocalDate fromDate, LocalDate toDate, Employee employee);
}
