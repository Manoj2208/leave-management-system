package com.hcl.leavemanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.leavemanagement.entity.Employee;

public interface EmployeeRepository extends JpaRepository<Employee,Long>{

}
