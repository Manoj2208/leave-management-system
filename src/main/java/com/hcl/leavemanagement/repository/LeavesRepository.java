package com.hcl.leavemanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.leavemanagement.entity.Employee;
import com.hcl.leavemanagement.entity.Leaves;

public interface LeavesRepository extends JpaRepository<Leaves, Long> {
	Leaves findByEmployee(Employee employee);
}
