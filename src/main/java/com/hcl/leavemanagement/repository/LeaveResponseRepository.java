package com.hcl.leavemanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.leavemanagement.entity.LeaveResponse;

public interface LeaveResponseRepository extends JpaRepository<LeaveResponse, Long>{
	
}
