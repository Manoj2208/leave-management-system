package com.hcl.leavemanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.leavemanagement.entity.LeaveType;

public interface LeaveTypeRepository extends JpaRepository<LeaveType,Long> {

}
