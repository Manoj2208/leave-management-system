package com.hcl.leavemanagement.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.leavemanagement.dto.ApiResponse;
import com.hcl.leavemanagement.dto.ManagerResponseDto;
import com.hcl.leavemanagement.service.LeaveResponseService;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1")
public class LeaveResponseController {
	private final LeaveResponseService leaveResponseService;
	@PostMapping
	public ResponseEntity<ApiResponse> leaveResponse(@RequestBody ManagerResponseDto managerResponseDto){
		return new ResponseEntity<>(leaveResponseService.ManagerLeaveResponse(managerResponseDto),HttpStatus.CREATED);
	}
}
