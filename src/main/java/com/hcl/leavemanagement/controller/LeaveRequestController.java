package com.hcl.leavemanagement.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.leavemanagement.dto.ApiResponse;
import com.hcl.leavemanagement.dto.LeaveRequestDto;
import com.hcl.leavemanagement.service.LeaveRequestService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/v1/leaves")
@RequiredArgsConstructor
public class LeaveRequestController {
	private final LeaveRequestService leaveRequestService;

	@PostMapping
	public ResponseEntity<ApiResponse> applyLeave(@RequestBody LeaveRequestDto leaveRequestDto) {
		return ResponseEntity.status(HttpStatus.CREATED).body(leaveRequestService.applyLeave(leaveRequestDto));
	}
}
