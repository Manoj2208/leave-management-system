package com.hcl.leavemanagement.service.impl;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.hcl.leavemanagement.dto.ApiResponse;
import com.hcl.leavemanagement.dto.LeaveRequestDto;
import com.hcl.leavemanagement.entity.Employee;
import com.hcl.leavemanagement.entity.LeaveRequest;
import com.hcl.leavemanagement.entity.Leaves;
import com.hcl.leavemanagement.entity.Type;
import com.hcl.leavemanagement.exception.EmployeeNotFound;
import com.hcl.leavemanagement.exception.InvalidDate;
import com.hcl.leavemanagement.exception.LeaveRequestConflictException;
import com.hcl.leavemanagement.repository.EmployeeRepository;
import com.hcl.leavemanagement.repository.LeaveRequestRepository;
import com.hcl.leavemanagement.repository.LeavesRepository;
import com.hcl.leavemanagement.service.LeaveRequestService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class LeaveRequestServiceImpl implements LeaveRequestService {

	private final EmployeeRepository employeeRepository;

	private final LeavesRepository leavesRepository;
	private final LeaveRequestRepository leaveRequestRepository;
	Integer noOfAnnualLeaves;
	Integer noOfMyLeaves;
	long annualLeave;
	long myLeave;

	@Override
	public ApiResponse applyLeave(LeaveRequestDto leaveRequestDto) {
		Employee employee = employeeRepository.findById(leaveRequestDto.sapId()).orElseThrow(() -> {
			log.error("Invalid sapId throwed Employee Not Found Exception");
			throw new EmployeeNotFound("Employee Not Found");
		});
		Employee manager = employeeRepository.findById(employee.getReportsTo()).orElseThrow(() -> {
			log.error("Invalid sapId of Reporting manager");
			throw new EmployeeNotFound("Manager not found");
		});

		leaveRequestDto.leaveRequests().forEach(leaveTypeDto -> {
			LocalDate date1 = LocalDate.parse(leaveTypeDto.fromDate());
			LocalDate date2 = LocalDate.parse(leaveTypeDto.toDate());
			Optional<LeaveRequest> leaveRequest = leaveRequestRepository.findByFromDateOrToDateAndEmployee(date1, date2,
					employee);
			if (leaveRequest.isPresent()) {
				log.error("Duplicating Leave Request");
				throw new LeaveRequestConflictException("Leave already applied for the date");
			}
			if (date1.isAfter(employee.getRegisteredDate())) {
				if (leaveTypeDto.leaveType().equals(Type.AnnualLeave)) {
					log.info("annualLeave calculated");
					annualLeave = ChronoUnit.DAYS.between(date1, date2) + 1;
					log.info("" + annualLeave);
				} else {
					log.info("my leave calculated");
					myLeave = ChronoUnit.DAYS.between(date1, date2) + 1;
					log.info("" + myLeave);
				}
			} else
				throw new InvalidDate("Invalid Date Selected");
		});

		Leaves leaves = leavesRepository.findByEmployee(employee);

		leaves.getLeaveType().forEach(leaveType -> {
			if (leaveType.getType().equals(Type.AnnualLeave) && leaveType.getNoOfLeave() > annualLeave) {
				leaveType.setNoOfLeave(leaveType.getNoOfLeave() - (int) annualLeave);
			} else if (leaveType.getType().equals(Type.MyLeave) && leaveType.getNoOfLeave() > myLeave)
				leaveType.setNoOfLeave(leaveType.getNoOfLeave() - (int) myLeave);
			else {
				log.error("Leaves Requested are out of boound");
				throw new InvalidDate("No Leaves are pending to avail");
			}
		});

		List<LeaveRequest> leaveRequests = new ArrayList<>();
		leaveRequestDto.leaveRequests().forEach(leaveRequest -> {
			LocalDate date1 = LocalDate.parse(leaveRequest.fromDate());
			LocalDate date2 = LocalDate.parse(leaveRequest.toDate());
			LeaveRequest leaveReq = LeaveRequest.builder().employee(employee).manager(manager).fromDate(date1)
					.toDate(date2).leaveType(leaveRequest.leaveType()).reason(leaveRequest.reason()).build();
			leaveRequests.add(leaveReq);
		});
		leaveRequestRepository.saveAll(leaveRequests);
		leavesRepository.save(leaves);
		log.info("leaves applied");
		return ApiResponse.builder().message("Leave Applied Successfully").httpStatus(201l).build();
	}

}
