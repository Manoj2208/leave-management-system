package com.hcl.leavemanagement.service.impl;

import java.time.LocalDate;
import java.util.List;

import org.springframework.stereotype.Service;

import com.hcl.leavemanagement.dto.ApiResponse;
import com.hcl.leavemanagement.dto.ManagerResponseDto;
import com.hcl.leavemanagement.entity.Employee;
import com.hcl.leavemanagement.entity.LeaveRequest;
import com.hcl.leavemanagement.entity.LeaveResponse;
import com.hcl.leavemanagement.entity.LeaveType;
import com.hcl.leavemanagement.entity.Status;
import com.hcl.leavemanagement.exception.EmployeeNotFound;
import com.hcl.leavemanagement.exception.InvalidDate;
import com.hcl.leavemanagement.exception.RequestIdNotFound;
import com.hcl.leavemanagement.exception.UnAuthorisedException;
import com.hcl.leavemanagement.repository.EmployeeRepository;
import com.hcl.leavemanagement.repository.LeaveRequestRepository;
import com.hcl.leavemanagement.repository.LeaveResponseRepository;
import com.hcl.leavemanagement.repository.LeaveTypeRepository;
import com.hcl.leavemanagement.repository.LeavesRepository;
import com.hcl.leavemanagement.service.LeaveResponseService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
@Service
@RequiredArgsConstructor
@Slf4j
public class LeaveResponseServiceImpl implements LeaveResponseService {
	private final EmployeeRepository employeeRepository;
	private final LeaveResponseRepository leaveResponseRepository;
	private final LeaveRequestRepository leaveRequestRepository;
	private final LeavesRepository leavesRepository;
	private final LeaveTypeRepository leaveTypeRepository;
	
	
	@Override
	public ApiResponse ManagerLeaveResponse(ManagerResponseDto managerResponseDto) {
		Employee  manager=employeeRepository.findById(managerResponseDto.sapId()).orElseThrow(EmployeeNotFound::new);
		                 managerResponseDto.requestId();
		LeaveRequest  leaveRequest=leaveRequestRepository.findById(managerResponseDto.requestId()).orElseThrow(RequestIdNotFound::new);
		
		
		Employee employee=leaveRequest.getEmployee();
		LocalDate toDate=leaveRequest.getToDate();
		LocalDate fromDate=leaveRequest.getFromDate();
		
		if(!manager.getSapId().equals(employee.getReportsTo())) {
			log.warn("UnAuthorisedException");
			throw new UnAuthorisedException();
		}
		
		
		List<LeaveResponse> leaveResponses=managerResponseDto.leaveResponseDtos().stream().map(response->{
			LocalDate date=LocalDate.parse(response.date());
			if(!date.isAfter(fromDate) && !date.isBefore(toDate) && !date.equals(fromDate) && !date.equals(toDate)) {
				log.warn("InvalidDate");
				throw new InvalidDate();
			}
			LeaveResponse leaveResponse=new LeaveResponse();
			leaveResponse.setDate(date);
			leaveResponse.setLeaveRequestId(managerResponseDto.requestId());
			leaveResponse.setStatus(response.status());
			
				
			return leaveResponse;
			}).toList();
		
		
		
		
		int count=(int) managerResponseDto.leaveResponseDtos().stream().filter(leaves->
			
			leaves.status().equals(Status.Approved)
		).count();
		
		
		
		
	    List<LeaveType> leaveTypes=leavesRepository.findByEmployee(employee).getLeaveType().stream()
	    		
	    		.filter(leaveType->
	    	
			leaveType.getType().equals(leaveRequest.getLeaveType())
			
		)
	    		
	    		.map(leaveType->{
	    	log.info("Calculate Number of Approved Leaves");
			
			leaveType.setNoOfLeave(leaveType.getNoOfLeave()-count);
			
			return leaveType;
			
		}).toList();
		
		
		
		
		
		
		leaveTypeRepository.saveAll(leaveTypes);
	    leaveResponseRepository.saveAll(leaveResponses);
		
		
		log.info("Manager Responded on applied leaves");
		
		return new ApiResponse("Manager Responded on applied leaves", 201L);
	}

}
