package com.hcl.leavemanagement.service;

import com.hcl.leavemanagement.dto.ApiResponse;
import com.hcl.leavemanagement.dto.ManagerResponseDto;

public interface LeaveResponseService {
	ApiResponse ManagerLeaveResponse(ManagerResponseDto managerResponseDto);
}
