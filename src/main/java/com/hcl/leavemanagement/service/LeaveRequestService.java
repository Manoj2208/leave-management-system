package com.hcl.leavemanagement.service;

import com.hcl.leavemanagement.dto.ApiResponse;
import com.hcl.leavemanagement.dto.LeaveRequestDto;

public interface LeaveRequestService {

	ApiResponse applyLeave(LeaveRequestDto leaveRequestDto);

}
