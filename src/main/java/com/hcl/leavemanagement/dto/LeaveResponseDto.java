package com.hcl.leavemanagement.dto;

import com.hcl.leavemanagement.entity.Status;

import lombok.Builder;
@Builder
public record LeaveResponseDto(String date,Status status) {

}
