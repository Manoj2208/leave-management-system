package com.hcl.leavemanagement.dto;

import com.hcl.leavemanagement.entity.Type;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import lombok.Builder;

@Builder
public record LeaveTypeDto(@NotBlank(message = "LeaveType is required") Type leaveType,
		@NotBlank(message = "fromDate is required") @Pattern(regexp = "\\d{4}-\\d{2}-\\d{2}", message = "invalid fromDate format") String fromDate,
		@NotBlank(message = "toDate is required") @Pattern(regexp = "\\d{4}-\\d{2}-\\d{2}", message = "invalid toDate format") String toDate,
		@NotBlank(message = "reason is required") String reason) {

}
