package com.hcl.leavemanagement.dto;

import java.util.List;

import lombok.Builder;

@Builder
public record ManagerResponseDto(Long sapId,Long requestId,List<LeaveResponseDto> leaveResponseDtos) {

}
