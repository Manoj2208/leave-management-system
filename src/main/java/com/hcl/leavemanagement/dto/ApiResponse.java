package com.hcl.leavemanagement.dto;

import lombok.Builder;

@Builder
public record ApiResponse(String message,Long httpStatus) {

}
