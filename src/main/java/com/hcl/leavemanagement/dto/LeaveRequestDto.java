package com.hcl.leavemanagement.dto;

import java.util.List;

import jakarta.validation.Valid;
import lombok.Builder;

@Builder
public record LeaveRequestDto(Long sapId, @Valid List<LeaveTypeDto> leaveRequests) {

}
