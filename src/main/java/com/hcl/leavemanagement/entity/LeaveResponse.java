package com.hcl.leavemanagement.entity;

import java.time.LocalDate;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LeaveResponse {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long leaveResponseId;
	private Long leaveRequestId;
	private LocalDate date;
	@Enumerated(EnumType.STRING)
	private Status status;
}
